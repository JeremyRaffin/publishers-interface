/* Main js */

$(document).ready(function() {

	// Activation menu
	$('#buttonMenu').click(function() {
		$('.page-wrapper').toggleClass('js-transform');
		$('.header').toggleClass('js-transform');
		$('body').toggleClass('js-mobile-nav-active');
		return false;
	});

	// On désactive le menu lors d'un clic 
	$('#aside').click(function() {
		$('body').removeClass('js-mobile-nav-active');
		$('.page-wrapper').removeClass('js-transform');
		$('.header').removeClass('js-transform');
	});

    /*('.program-more-info-button').click(function() {
        alert('Click');
        $('.program-more-info').toggleClass('hidden');
        $('.program-more-info').toggleClass('show');
        return false;
    });*/

    
    var allPanels = $('.program-more-info').hide();
    var firstPanel = $('.program-more-info').first().show();

    $('.program-info').click(function() {
        allPanels.slideUp();
        //firstPanel.slideDown();
        $(this).next().slideDown();
        return false;
    });

});

var chart = c3.generate({
	bindto: '#chart',
	size: {
        height: 336
    },
    data: {
        x: 'x',
        columns: [
            ['x', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'],
            ['en cours', '487', '501', '140', '321', '181', '100', '40', '70', '81', '30', '30', '40', '40', '43', '44', '16', '43', '298', '566', '217'],
            ['précédent', '1111', '1312', '250', '14', '24', '502', '564', '307', '198', '160', '97', '694', '247', '174', '42', '771', '1396', '3511', '1729', '492']
        ]
    },

     grid: {
        x: {
            show: true
        },
        y: {
            show: true
        }
    }
});

var chartArea = c3.generate({
	bindto: '#chartArea',
    data: {
        columns: [
            ['data1', 300, 350, 300, 0, 0, 0],
            ['data2', 130, 100, 140, 200, 150, 50],
            ['data3', 200, 75, 150, 25, 125, 80],
            ['data4', 75, 30, 260, 250, 140, 150],
        ],
        types: {
            data1: 'area',
            data2: 'area',
            data3: 'area',
            data4: 'area'
        }
    }
});


var barChart = c3.generate({
	bindto : '#barChart',
    data: {
        columns: [
            ['en cours', 30, 200, 100, 400, 150, 250],
            ['précédent', 130, 100, 140, 200, 150, 50]
        ],
        type: 'bar'
    },
    bar: {
        width: {
            ratio: 0.5 // this makes bar width 50% of length between ticks
        }
        // or
        //width: 100 // this makes bar width 100px
    }
});